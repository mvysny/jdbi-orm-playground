package playground;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static playground.DatabaseUtils.h2;

/**
 * @author mavi
 */
public class H2Test {
    @Test
    public void h2Test() {
        h2(() -> {
            new Person("Jon Lord", 42).save();
            assertArrayEquals(new Object[] { "Jon Lord"},
                    Person.dao.findAll().stream().map(it -> it.getName()).toArray());
        });
    }
}
