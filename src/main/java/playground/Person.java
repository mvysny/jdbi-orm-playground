package playground;

import com.gitlab.mvysny.jdbiorm.Dao;
import com.gitlab.mvysny.jdbiorm.Entity;
import org.hibernate.validator.constraints.Length;
import org.jdbi.v3.core.mapper.reflect.ColumnName;
import org.jetbrains.annotations.Nullable;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * An example entity.
 */
public class Person implements Entity<Long> {
    private Long id;
    @Length(min = 1)
    private String name;
    private Integer age;
    private LocalDate dateOfBirth;
    private Instant created;
    private Instant modified;
    @ColumnName("alive")
    private Boolean isAlive;
    private MaritalStatus maritalStatus;

    public enum MaritalStatus {
        Single,
        Married,
        Divorced,
        Widowed
    }

    public Person() {
    }

    public Person(@Length(min = 1) String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    @Nullable
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public Instant getModified() {
        return modified;
    }

    public void setModified(Instant modified) {
        this.modified = modified;
    }

    public Boolean getAlive() {
        return isAlive;
    }

    public void setAlive(Boolean alive) {
        isAlive = alive;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", dateOfBirth=" + dateOfBirth +
                ", created=" + created +
                ", modified=" + modified +
                ", isAlive=" + isAlive +
                ", maritalStatus=" + maritalStatus +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public void save(boolean validate) {
        if (id == null) {
            created = Instant.now();
        }
        modified = Instant.now();
        Entity.super.save(validate);
    }

    public static final Dao<Person, Long> dao = new Dao<>(Person.class);
}
