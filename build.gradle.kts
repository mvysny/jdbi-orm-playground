plugins {
    java
    application
}

defaultTasks("clean", "build")

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.gitlab.mvysny.jdbiorm:jdbi-orm:2.9")
    implementation("com.zaxxer:HikariCP:5.1.0")
    implementation("org.slf4j:slf4j-simple:2.0.16")

    // validation support
    implementation("org.hibernate.validator:hibernate-validator:8.0.1.Final")
    // EL is required: http://hibernate.org/validator/documentation/getting-started/
    implementation("org.glassfish:jakarta.el:4.0.2")

    // database drivers
    implementation("com.h2database:h2:2.2.224")
    implementation("org.postgresql:postgresql:42.7.2")
    implementation("mysql:mysql-connector-java:8.0.30")
    implementation("org.mariadb.jdbc:mariadb-java-client:3.0.6")
    implementation("com.microsoft.sqlserver:mssql-jdbc:11.2.1.jre8")

    // tests
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.11.0")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

application {
    mainClass.set("playground.Main")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}
